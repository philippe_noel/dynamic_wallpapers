#!/usr/bin/env bash

set -uo pipefail
IFS=$'\n\t'

cmdname=$(basename "$0")

## Global variables
DIRECTORY=""
ONCE=0

function usage() {
  yellow=$(tput setaf 3)
  green=$(tput setaf 2)
  normal=$(tput sgr0)

  cat >&2 << USAGE
${green}Dynamic wallpaper${normal}
Use images stored in a folder and set them as wallpaper
The wallpaper is changed frequently depending on the number of wallpaper in the folder.
For example, if there are 12 images in the folder, the wallpaper will be change every 2 hours (24 hours of a day / 12 images)

${green}Usage:${normal}
    ${cmdname} --folder <folder_name>
${green}Options:${normal}
    ${yellow}-h, --help${normal}      Print this help message
    ${yellow}-d, --debug${normal}     Enable the debug version of the script
    ${yellow}-o, --once${normal}      Change the wallpaper according to the clock and stop

    ${yellow}-f, --folder${normal}    Folder containing images
USAGE
}

function info() {
  echo "$cmdname: [Info] $*";
}

function exit_if_error() {
  ## Print the message on the stderr and exit the script with error code
  ##
  ## $1 : Exit code to print and to return
  ## $2 : Message to print to stderr
  _error "$1" 1 "Err" "$2"
}

function warn_if_error() {
  ## Print the message on the stderr with a warning
  ##
  ## $1 : Exit code to print and to return
  ## $2 : Message to print to stderr
  _error "$1" 0 "Warn" "$2"
}

function _error() {
  ## Print an Error if the exit_code is != 0
  ## Exit the program if exit -eq 0
  ##
  ## $1 : exit code (don't do anything if error code = 0)
  ## $2 : must be 1 or 0. If set to 1, exit the program
  ## $3 : tag to print (for example Err, Warn, ...)
  ## $* : Error message
  local exit_code="$1"
  local exit_bool="$2"
  local exit_tag="$3"

  shift 3
  [[ $exit_code ]] &&               # do nothing if no error code passed
    ((exit_code != 0)) && {         # do nothing if error code is 0
      echo >&2 "$cmdname: [${exit_tag}] $*";
      [ "$exit_bool" -eq 1 ] && exit "$exit_code"
    }
}

function check_command() {
  ## Check if a command exists in the $PATH
  ##
  ## $1 : command to test
  command -v "$1" > /dev/null 2>&1 || exit_if_error 1 "'$1' commmand does not exist in the PATH. Please install it before continue"
}

function check_input_dir() {
  ## Check if the input dir is set and exists
  ##
  ## $1 : input directory
  local my_dir="$1"

  [ ! -z "${my_dir}" ] || exit_if_error $? "Set a directory containing images with -f/--folder option"
  [ -d "${my_dir}" ] || exit_if_error $? "'${my_dir}' is not a directory"
}

## Check if all commands exists
check_command "feh"

TMP_CL_OPTION=$(getopt -o f:,h,d,o -l "folder:,help,debug,once" -n "$cmdname" -- "$@")

[ $? -eq 0 ] || exit_if_error 1 "Argument does not match ($TMP_CL_OPTION)"

## If no argument provide, print help
[ ! $# -eq 0 ] || {
  warn_if_error 1 "Please use an argument"
  usage
  exit 0
}

eval set -- "$TMP_CL_OPTION"

while true; do
  case "$1" in
    -h|--help) usage; exit 0 ;;
    -f|--folder ) DIRECTORY="$2"; shift 2 ;;
    -d|--debug ) set -x; shift ;;
    -o|--once ) ONCE=1; shift ;;
    --) shift; break ;;
    *) exit_if_error 1 "Unexpected $1" ;;
  esac
done

## Wait to the Xdisplay to start TODO: Write a test here
sleep 5

## Is directory set and exists
DIRECTORY=$(realpath "${DIRECTORY}")
check_input_dir "$DIRECTORY"

# Store all wallpaper in an array
declare -a file_array=($(ls -1 $DIRECTORY | sort -t _ -k 2 -g))
nb_files=${#file_array[@]}

# Time (in hours) between each wallpaper changes
time_between_changes=$((24/nb_files))

# Use the Epoch time (to limit number of seconds and simplify debugging)
# Set the begin at 00:00:00
# Dependeing on time zone but in UTC+0200, init_date = -3600
init_date=$(date -d "1970-01-01 00:00:00" +%s)

# Convert the current time (date still Epoch) in seconds
current_seconds=$(date -d "1970-01-01 $(date +%T)" +%s)

# Compute which wallpaper should be print
current_wallpaper=$(echo "($current_seconds - $init_date)/(3600*$time_between_changes)" | bc)

feh --bg-scale "$DIRECTORY"/"${file_array[$current_wallpaper]}" || exit_if_error $? "Error with feh. Maybe Xdisplay is not ready ?"

## Stop the program here if --once option is used
[ $ONCE -eq 1 ] && exit 0

# Compute when at which hours the next changes will occur
hours_to_change=$(echo "($current_wallpaper + 1)*$time_between_changes" | bc)

# Compute the time in seconds between NOW and the next changement
time_remaining=$(($(date -d "1970-01-01 $hours_to_change:00:00" +%s) - $current_seconds))

sleep $time_remaining

# Infinite loop.
# Change the wallpaper
while true; do
      if [ $current_wallpaper -eq $(($nb_files - 1)) ]; then
          current_wallpaper=0
      else
            current_wallpaper=$(($current_wallpaper + 1))
      fi
      feh --bg-scale $DIRECTORY/${file_array[$current_wallpaper]}
      echo $(date)
      echo "Change wallpaper $DIRECTORY/${file_array[$current_wallpaper]}"
      sleep $(($time_between_changes*3600))
done
